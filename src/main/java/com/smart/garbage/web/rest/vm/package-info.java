/**
 * View Models used by Spring MVC REST controllers.
 */
package com.smart.garbage.web.rest.vm;
