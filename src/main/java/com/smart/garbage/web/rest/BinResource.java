package com.smart.garbage.web.rest;

import com.smart.garbage.domain.Bin;
import com.smart.garbage.service.BinService;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api/bin")
public class BinResource {

    private final BinService binService;

    public BinResource(BinService binService) {
        this.binService = binService;
    }

    @PostMapping(path = "")
    public ResponseEntity<Bin> saveBin(@RequestBody Bin bin) {
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(binService.saveBin(bin)));
    }

    @GetMapping(path = "")
    public Page<Bin> getBinsPage(Pageable pageable) {
        return binService.getBinsPage(pageable);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Bin> getBinById(@PathVariable String id) {
        return ResponseUtil.wrapOrNotFound(binService.getBinById(id));
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable String id) {
        binService.deleteBin(id);
    }
}
