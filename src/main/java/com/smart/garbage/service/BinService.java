package com.smart.garbage.service;

import com.smart.garbage.domain.Bin;
import com.smart.garbage.repository.BinRepository;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BinService {

    private final BinRepository binRepository;

    public BinService(BinRepository binRepository) {
        this.binRepository = binRepository;
    }

    public Bin saveBin(Bin bin) {
        return binRepository.save(bin);
    }

    public Page<Bin> getBinsPage(Pageable pageable) {
        return binRepository.findAllBy(pageable);
    }

    public Optional<Bin> getBinById(String id) {
        return binRepository.findById(id);
    }

    public void deleteBin(String id) {
        binRepository.findById(id).ifPresent(binRepository::delete);
    }
}
