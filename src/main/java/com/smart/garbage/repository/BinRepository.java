package com.smart.garbage.repository;

import com.smart.garbage.domain.Bin;
import com.smart.garbage.domain.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the {@link User} entity.
 */
@Repository
public interface BinRepository extends MongoRepository<Bin, String> {
    Optional<Bin> findBinById(String id);

    Page<Bin> findAllBy(Pageable pageable);
}
